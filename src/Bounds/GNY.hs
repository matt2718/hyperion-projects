{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE LambdaCase             #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE MultiWayIf             #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TupleSections          #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Bounds.GNY where

import qualified Bounds.FourFermions3d                  as FFFF
import           Control.Monad.IO.Class                 (liftIO)
import           Data.Aeson                             (FromJSON, FromJSONKey,
                                                         ToJSON, ToJSONKey)
import           Data.Binary                            (Binary)
import           Data.Data                              (Typeable)
import           Data.Matrix.Static                     (Matrix)
import           Data.Proxy                             (Proxy (..))
import           Data.Reflection                        (Reifies, reflect)
import           Data.Tagged                            (Tagged)
import           Data.Traversable                       (for)
import           Data.Vector                            (Vector)
import           GHC.Generics                           (Generic)
import           GHC.TypeNats                           (KnownNat)
import           Hyperion                               (Dict (..), Static (..),
                                                         cPtr)
import           Hyperion.Bootstrap.CFTBound            (BuildInJob,
                                                         SDPFetchBuildConfig (..),
                                                         ToSDP (..), blockDir)
import           Linear.V                               (V)
import           SDPB.Blocks                            (Block (..),
                                                         BlockFetchContext,
                                                         Coordinate (XT),
                                                         CrossingMat,
                                                         Delta (..),
                                                         Derivative (..),
                                                         TaylorCoeff (..),
                                                         Taylors, unzipBlock,
                                                         xEven, xOdd, xtTaylors,
                                                         yEven, yOdd)
import qualified SDPB.Blocks.Blocks3d                   as B3d
import           SDPB.Blocks.Blocks3d.Build             (block3dBuildLink)
import qualified SDPB.Bounds.BootstrapSDP               as BSDP
import           SDPB.Bounds.BoundDirection             (BoundDirection,
                                                         boundDirSign)
import           SDPB.Bounds.Crossing.CrossingEquations (FourPointFunctionTerm,
                                                         HasRep (..),
                                                         OPECoefficient,
                                                         OPECoefficientExternal,
                                                         ThreePointStructure (..),
                                                         crossingMatrix,
                                                         crossingMatrixExternal,
                                                         derivsVec, map4pt,
                                                         mapBlocksFreeVect,
                                                         mapOps,
                                                         opeCoeffGeneric_,
                                                         opeCoeffIdentical_,
                                                         runTagged2)
import           SDPB.Bounds.Spectrum                   (DeltaRange, Spectrum,
                                                         listDeltas)
import           SDPB.Build                             (FetchConfig (..),
                                                         SomeBuildChain (..),
                                                         noDeps)
import           SDPB.Math.FreeVect                     (FreeVect, vec)
import qualified SDPB.Math.FreeVect                     as FV
import           SDPB.Math.HalfInteger                  (HalfInteger)
import qualified SDPB.Math.HalfInteger                  as HI
import           SDPB.Math.Linear.Literal               (toV)
import qualified SDPB.Math.Linear.Util                  as L
import           SDPB.Math.VectorSpace                  (bilinearPair,
                                                         directSum, zero, (*^))
import qualified SDPB.SDP.Types                         as SDP

data ExternalOp s = Sig | Psi
  deriving (Show, Eq, Ord, Enum, Bounded)

data ExternalDims = ExternalDims
  { deltaSig :: Rational
  , deltaPsi :: Rational
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

instance (Reifies s ExternalDims) => HasRep (ExternalOp s) (B3d.ConformalRep Rational, ONRep) where
  rep x = case x of
    Sig -> (B3d.ConformalRep dSig 0,     ONSinglet)
    Psi -> (B3d.ConformalRep dPsi (1/2), ONVector)
    where
      ExternalDims { deltaSig = dSig, deltaPsi = dPsi} = reflect x

-- | A three-point structure label for Lorentz x O(N) flavor. Because
-- flavor 3pt structures in this model are uniquely determined by the
-- flavor representations, this is just a newtype over an SO(3) label.
newtype Lorentz_ON_Struct = Lorentz_ON_Struct B3d.SO3StructLabel
  deriving (Eq, Ord)

-- | An instance for creating a Lorentz x O(N) three-point structure
-- from a set of representations and an Lorentz_ON_Struct.
instance ThreePointStructure
  (B3d.SO3Struct a a b, ON3PtStruct n)
  Lorentz_ON_Struct
  (B3d.ConformalRep a, ONRep)
  (B3d.ConformalRep b, ONRep) where
  makeStructure (l1,r1) (l2,r2) (l3,r3) (Lorentz_ON_Struct so3Struct) =
    (makeStructure l1 l2 l3 so3Struct, ON3PtStruct r1 r2 r3)

instance ThreePointStructure
  (B3d.SO3Struct Rational Rational Delta, ON3PtStruct n)
  Lorentz_ON_Struct
  (B3d.ConformalRep Rational, ONRep)
  (B3d.ConformalRep Rational, ONRep) where
  makeStructure (l1,r1) (l2,r2) (l3,r3) (Lorentz_ON_Struct so3Struct) =
    (makeStructure l1 l2 (l3 { B3d.delta = Fixed (B3d.delta l3) }) so3Struct, ON3PtStruct r1 r2 r3)

data ChannelType = ChannelType HalfInteger B3d.Parity ONRep
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON, ToJSONKey, FromJSONKey)

data GNY = GNY
  { externalDims :: ExternalDims
  , nGroup       :: Rational
  , spectrum     :: Spectrum ChannelType
  , objective    :: Objective
  , spins        :: [HalfInteger]
  , blockParams  :: B3d.Block3dParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility
  | StressTensorOPEBound BoundDirection
  | ExternalOPEBound BoundDirection
  | ExternalOPEBoundEps BoundDirection Rational (V 2 Rational)
  | FeasibilityEps Rational (V 2 Rational)
  | FeasibilityExtEps Rational (V 3 Rational)
  -- | ConservedCurrentOPEBound BoundDirection
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

-- | A channel with crossing matrices of size jxj containing blocks of
-- type 'b'
data Channel j b where
  Scalar_ParityEven_Singlet        :: Delta                  -> Channel 2 B3d.Block3d
  SpinEven_ParityEven_Singlet      :: B3d.ConformalRep Delta -> Channel 3 B3d.Block3d
  SpinEven_ParityOdd_Singlet       :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  SpinOdd_ParityOdd_Singlet        :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  Scalar_ParityEven_TensorSym      :: Delta                  -> Channel 1 B3d.Block3d
  SpinEven_ParityEven_TensorSym    :: B3d.ConformalRep Delta -> Channel 2 B3d.Block3d
  SpinEven_ParityOdd_TensorSym     :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  SpinOdd_ParityOdd_TensorSym      :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  SpinOdd_ParityEven_TensorAntiSym :: B3d.ConformalRep Delta -> Channel 2 B3d.Block3d
  SpinOdd_ParityOdd_TensorAntiSym  :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  SpinEven_ParityOdd_TensorAntiSym :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  ParityEven_Vector                :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  ParityOdd_Vector                 :: B3d.ConformalRep Delta -> Channel 1 B3d.Block3d
  IdentityChannel     :: Channel 1 B3d.IdentityBlock3d
  StressTensorChannel :: Channel 1 B3d.Block3d
  CurrentChannel      :: Channel 2 B3d.Block3d
  ExternalOpChannel   :: Channel 1 B3d.Block3d
  ExternalEpsChannel  :: Delta -> Channel 3 B3d.Block3d

-- | Data needed for "bulk" positivity conditions not associated with
-- external operators, the identity or the stress tensor.
data BulkConstraint where
  BulkConstraint
    :: KnownNat j -- ^ Existentially quantify over j
    => DeltaRange -- ^ Isolated or Continuum constraint
    -> Channel j B3d.Block3d -- ^ The associated Channel
    -> BulkConstraint

------------------ O(N) representation theory ---------------------

-- | Here, we only implement the O(N) representation theory needed for
-- this problem, which involves four-point functions of O(N) singlets
-- and vectors.

-- | O(N) representations appearing in correlators of singlets and
-- vectors
data ONRep = ONSinglet | ONVector | ONSymTensor | ONAntiSymTensor
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON,Bounded,Enum)

-- | Flavor structures for a four-point function of SO(N) vectors,
-- from Aike's notes. Here v_{ij} = v_i.v_j with v_j an SO(N)
-- polarization vector:
--
--  QPlus  = v_{12} v_{34} + v_{23} v_{14} (crossing even)
--  QMinus = v_{12} v_{34} - v_{23} v_{14} (crossing odd)
--  Q3     = v_{13} v_{24}                 (crossing even)
--
--  Unique plays two roles: as v_i.v_j in a four-point function
--  containing two vector's, and as 1 in a four-point function of
--  singlets.
data ON4PtStruct n = QPlus | QMinus | Q3 | Unique
  deriving (Eq, Ord)

-- | For the ONReps we allow here, there is a unique three-point
-- structure for each allowed triplet of representations. Thus, the
-- 3-point structures are just labeled by the representations.
data ON3PtStruct n = ON3PtStruct ONRep ONRep ONRep
  deriving (Eq, Ord)

-- | Flavor block for the exchange of the given 'ONRep' in a
-- four-point function of O(N) vectors.
onVectorBlock
  :: forall n a . (Reifies n Rational, Fractional a, Eq a)
  => ONRep
  -> FreeVect (ON4PtStruct n) a
onVectorBlock r = case r of
  ONSinglet       -> v12_34
  ONSymTensor     -> 1/2 *^ (v13_24 + v23_14) - 1/nGroup *^ v12_34
  -- | Fixed a sign here, since the block should come from merging
  -- (120) and (430) -- double check this!
  ONAntiSymTensor -> 1/2 *^ (v23_14 - v13_24)
  _               -> 0
  where
    -- | vij_kl represents v_{ij} v_{kl}. Here, they can be obtained
    -- by inverting the definitions of QPlus, QMinus, Q3.
    v12_34 = 1/2 *^ vec QPlus + 1/2 *^ vec QMinus
    v23_14 = 1/2 *^ vec QPlus - 1/2 *^ vec QMinus
    v13_24 = vec Q3
    nGroup = fromRational (reflect @n Proxy)

-- | Evaluate the given O(N) flavor block to obtain a number
evalONBlock :: (Reifies n Rational, Fractional a, Eq a) => Block (ON3PtStruct n) (ON4PtStruct n) -> a
evalONBlock (Block (ON3PtStruct r1 r2 r) (ON3PtStruct r4 r3 r') f)
  | r /= r' = 0
  | otherwise = case ((r1, r2, r3, r4), r, f) of
      ((ONVector, ONVector, ONVector, ONVector), _, _) -> FV.coeff f (onVectorBlock r)
      ((ONSinglet, ONSinglet, ONSinglet, ONSinglet), ONSinglet, Unique) -> 1
      ((ONVector,  ONVector,  ONSinglet, ONSinglet), ONSinglet, Unique) -> 1
      ((ONSinglet, ONSinglet, ONVector,  ONVector),  ONSinglet, Unique) -> 1
      ((ONVector,  ONSinglet, ONVector,  ONSinglet), ONVector,  Unique) -> 1
      ((ONVector,  ONSinglet, ONSinglet, ONVector),  ONVector,  Unique) -> 1
      ((ONSinglet, ONVector,  ONVector,  ONSinglet), ONVector,  Unique) -> 1
      ((ONSinglet, ONVector,  ONSinglet, ONVector),  ONVector,  Unique) -> 1
      _ -> 0

------------------ Crossing equations ---------------------

-- | Crossing equations for a four-point function <psi psi s s> of two
-- fermions and two scalars, following David Poland's notes, equation
-- 1.16. Note that these equations are in a different order, as
-- indicated by the line number comments below.
crossingEqsFFSS
  :: forall s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
  -> V 6 (Taylors 'XT, FreeVect b a)
crossingEqsFFSS g0 = toV
  ( ( xtTaylors xOdd  yEven, g s s u u yEven + g u s s u yEven ) -- ^ line 2
  , ( xtTaylors xEven yEven, g s s u u yEven - g u s s u yEven ) -- ^ line 4
  , ( xtTaylors xOdd  yOdd,  g s s u u yOdd  - g u s s u yOdd )  -- ^ line 5
  , ( xtTaylors xEven yOdd,  g s s u u yOdd  + g u s s u yOdd )  -- ^ line 3
  , ( xtTaylors xOdd  yEven, g u s u s yEven )                   -- ^ line 6
  , ( xtTaylors xEven yOdd,  g u s u s yOdd )                    -- ^ line 7
  )
  where
    g (o1,q1) (o2,q2) (o3,q3) (o4,q4) ySign =
      g0 o1 o2 o3 o4 (B3d.Q4Struct (q1,q2,q3,q4) ySign)
    u = (Psi, 1/2)
    s = (Sig, 0)

-- | Crossing equation for a four-point function of scalars.
crossingEqSSSS
  :: forall s a b .
     FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
  -> V 1 (Taylors 'XT, FreeVect b a)
crossingEqSSSS g = toV
  (xtTaylors xOdd yEven, g Sig Sig Sig Sig (B3d.Q4Struct (0,0,0,0) yEven))

-- | Crossing equations of the GNY model with N /= 1
crossingEqsGNY
  :: forall n s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) (B3d.Q4Struct, ON4PtStruct n) b a
  -> V 22 (Taylors 'XT, FreeVect b a)
crossingEqsGNY g =
  FFFF.crossingEqsWithFlavorSign xEven (mapOps (const Psi) (map4pt (,QPlus)  g)) L.++
  FFFF.crossingEqsWithFlavorSign xEven (mapOps (const Psi) (map4pt (,Q3)     g)) L.++
  FFFF.crossingEqsWithFlavorSign xOdd  (mapOps (const Psi) (map4pt (,QMinus) g)) L.++
  crossingEqsFFSS (map4pt (,Unique) g) L.++
  crossingEqSSSS  (map4pt (,Unique) g)

-- | Crossing equations of the GNY model with N=1 (or the N=1 SUSY
-- Ising model, without SUSY taken into account)
crossingEqsN1
  :: forall s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
  -> V 12 (Taylors 'XT, FreeVect b a)
crossingEqsN1 g =
  FFFF.crossingEqsWithFlavorSign xEven (mapOps (const Psi) g) L.++
  crossingEqsFFSS g L.++
  crossingEqSSSS  g

derivsVecGNY :: GNY -> V 22 (Vector (TaylorCoeff (Derivative 'XT)))
derivsVecGNY f =
  fmap ($ B3d.nmax (blockParams f)) (derivsVec crossingEqsGNY)

crossingMatGNY
  :: forall j n s a. (Reifies n Rational, KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient (ExternalOp s) (B3d.SO3Struct Rational Rational Delta, ON3PtStruct n) a)
  -> Tagged '(n,s) (CrossingMat j 22 B3d.Block3d a)
crossingMatGNY channel =
  pure $ mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrix channel ((crossingEqsGNY @n @s))
  where
    evalFlavorCoeff (b,f) = evalONBlock f *^ vec (B3d.Block3d b)

so3 :: (Num a, Eq a) => HalfInteger -> HalfInteger -> FreeVect Lorentz_ON_Struct a
so3 j12 j123 = vec (Lorentz_ON_Struct (B3d.SO3StructLabel j12 j123))

-- | The following three-point structures are in accordance with
-- Luca's notes (updated on Aug 31, 2020)

mat
  :: forall j b n s a . (Reifies s ExternalDims, Reifies n Rational, Fractional a, Floating a, Eq a)
  => Channel j b
  ->  Tagged '(n,s) (CrossingMat j 22 b a)

-- Singlet r1 r2
mat (Scalar_ParityEven_Singlet deltaScalar) = crossingMatGNY $ toV
  ( opeCoeffIdentical_ Psi iRep (so3 0 0)
  , opeCoeffIdentical_ Sig iRep (so3 0 0)
  )
  where
    iRep = (B3d.ConformalRep deltaScalar 0, ONSinglet)

mat (SpinEven_ParityEven_Singlet iConformalRep) = crossingMatGNY $ toV
  ( opeCoeffIdentical_ Psi iRep (so3 0 l)
  , opeCoeffIdentical_ Psi iRep (so3 1 l)
  , opeCoeffIdentical_ Sig iRep (so3 0 l)
  )
  where
    iRep = (iConformalRep, ONSinglet)
    l = B3d.spin iConformalRep

-- Singlet r3
mat (SpinEven_ParityOdd_Singlet iConformalRep) = crossingMatGNY $ toV
  ( opeCoeffIdentical_ Psi iRep r3 )
  where
    iRep = (iConformalRep, ONSinglet)
    l = B3d.spin iConformalRep
    r3 = - sqrt (realToFrac l) *^ so3 1 (l-1) + sqrt (realToFrac l + 1) *^ so3 1 (l+1)

-- Singlet r4
mat (SpinOdd_ParityOdd_Singlet iConformalRep) = crossingMatGNY $ toV
  ( opeCoeffIdentical_ Psi iRep r4 )
  where
    iRep = (iConformalRep, ONSinglet)
    l = B3d.spin iConformalRep
    r4 = sqrt (realToFrac l + 1) *^ so3 1 (l-1) + sqrt (realToFrac l) *^ so3 1 (l+1)

-- Symmetric Tensor r1 r2
-- (Essentially the same as Singlet)
mat (Scalar_ParityEven_TensorSym deltaScalar) = crossingMatGNY $ toV
  ( opeCoeffIdentical_ Psi iRep (so3 0 0))
  where
    iRep = (B3d.ConformalRep deltaScalar 0, ONSymTensor)

mat (SpinEven_ParityEven_TensorSym iConformalRep) = crossingMatGNY $ toV
  ( opeCoeffIdentical_ Psi iRep (so3 0 l)
  , opeCoeffIdentical_ Psi iRep (so3 1 l)
  )
  where
    iRep = (iConformalRep, ONSymTensor)
    l = B3d.spin iConformalRep

-- Symmetric Tensor  r3
mat (SpinEven_ParityOdd_TensorSym iConformalRep) = crossingMatGNY $ toV
  ( opeCoeffIdentical_ Psi iRep r3 )
  where
    iRep = (iConformalRep, ONSymTensor)
    l = B3d.spin iConformalRep
    r3 = - sqrt (realToFrac l) *^ so3 1 (l-1) + sqrt (realToFrac l + 1) *^ so3 1 (l+1)

-- Symmetric Tensor  r4
mat (SpinOdd_ParityOdd_TensorSym iConformalRep) = crossingMatGNY $ toV
  ( opeCoeffIdentical_ Psi iRep r4 )
  where
    iRep = (iConformalRep, ONSymTensor)
    l = B3d.spin iConformalRep
    r4 = sqrt (realToFrac l + 1) *^ so3 1 (l-1) + sqrt (realToFrac l) *^ so3 1 (l+1)

-- Antisymmetric Tensor r1 r2 (exchange even and odd spin )
mat (SpinOdd_ParityEven_TensorAntiSym iConformalRep) = crossingMatGNY $ toV
  ( opeCoeffIdentical_ Psi iRep (so3 0 l)
  , opeCoeffIdentical_ Psi iRep (so3 1 l)
  )
  where
    iRep = (iConformalRep, ONAntiSymTensor)
    l = B3d.spin iConformalRep

-- Antisymmetric Tensor r3
mat (SpinOdd_ParityOdd_TensorAntiSym iConformalRep) = crossingMatGNY $ toV
  ( opeCoeffIdentical_ Psi iRep r3 )
  where
    iRep = (iConformalRep, ONAntiSymTensor)
    l = B3d.spin iConformalRep
    r3 = - sqrt (realToFrac l) *^ so3 1 (l-1) + sqrt (realToFrac l + 1) *^ so3 1 (l+1)

-- Antisymmetric Tensor r4
mat (SpinEven_ParityOdd_TensorAntiSym iConformalRep) = crossingMatGNY $ toV
  ( opeCoeffIdentical_ Psi iRep r4 )
  where
    iRep = (iConformalRep, ONAntiSymTensor)
    l = B3d.spin iConformalRep
    r4 = sqrt (realToFrac l + 1) *^ so3 1 (l-1) + sqrt (realToFrac l) *^ so3 1 (l+1)


-- psi * sigma
-- Flipped signs
mat (ParityEven_Vector iConformalRep) = crossingMatGNY $ toV
  ( opeCoeffGeneric_ Psi Sig iRep (so3 (1/2) (l-1/2)) ((-1)^(round (l+1/2) :: Int) *^ (so3 (1/2) (l+1/2)))
  )
  where
    iRep = (iConformalRep, ONVector)
    l = B3d.spin iConformalRep

mat (ParityOdd_Vector iConformalRep) = crossingMatGNY $ toV
  ( opeCoeffGeneric_ Psi Sig iRep (so3 (1/2) (l+1/2)) ((-1)^(round (l-1/2) :: Int) *^ (so3 (1/2) (l-1/2)))
  )
  where
    iRep = (iConformalRep, ONVector)
    l = B3d.spin iConformalRep


-- Identity
mat IdentityChannel =
  pure $ mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrix (toV identityOpe) (crossingEqsGNY @n @s)
  where
    evalFlavorCoeff (b,f) =  evalONBlock f *^ vec (B3d.IdentityBlock3d b)
    identityOpe o1 o2
      | o1 == o2  = vec ((fst o1rep, ON3PtStruct (snd o1rep) (snd o1rep) ONSinglet))
      | otherwise = 0
      where o1rep = rep @(ExternalOp s) o1


-- StressTensor
mat StressTensorChannel = crossingMatGNY (toV stressTensorOpe)
  where
    stressTensorRep = (B3d.ConformalRep (RelativeUnitarity 0) 2, ONSinglet)
    psiTStruct = - 3/(2*sqrt 2*pi) *^ so3 1 2 + (sqrt 3*dPsi)/(2*pi) *^ so3 0 2
    sigTStruct = - (sqrt 3*dSig)/(2*sqrt 2*pi) *^ so3 0 2
    dPsi = fromRational (deltaPsi (reflect @s Psi))
    dSig = fromRational (deltaSig (reflect @s Sig))
    stressTensorOpe o1 o2 =
      FV.mapBasis (makeStructure (rep o1) (rep o2) stressTensorRep) $
      case (o1, o2) of
        (Psi, Psi) -> psiTStruct
        (Sig, Sig) -> sigTStruct
        _          -> 0


-- Conserved Current
mat CurrentChannel = mat (SpinOdd_ParityEven_TensorAntiSym (fst currentRep))
  where
    currentRep = (B3d.ConformalRep (RelativeUnitarity 0) 1, ONAntiSymTensor)
    -- Not using the Ward identity for now
    -- psiTStruct = - 1/(sqrt 2*pi) *^ so3 0 1
    -- fixedCurrentOPE o1 o2
    --   | (o1 == o2 && o1 == Psi)  = FV.mapBasis (makeStructure (rep o1) (rep o1) (currentRep)) psiTStruct
    --   | otherwise = zero


-- External
mat ExternalOpChannel =
  pure . mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrixExternal opeCoefficients (crossingEqsGNY @n) [Sig,Psi]
  where
    evalFlavorCoeff (b,f) = evalONBlock f *^ vec (B3d.Block3d b)
    opeCoefficients :: V 1 (OPECoefficientExternal (ExternalOp s) (B3d.SO3Struct Rational Rational Delta, ON3PtStruct n) a)
    opeCoefficients = toV $ \o1 o2 o3 ->
      FV.mapBasis (makeStructure (rep o1) (rep o2) (rep o3)) $
      case (o1, o2, o3) of
        (Psi,Psi,Sig) -> - so3 1 1
        (Psi,Sig,Psi) -> so3 (1/2) 0
        (Sig,Psi,Psi) -> - so3 (1/2) 1 --Flipped sign
        _             -> 0

-- Block matrix of the external OPE channel as well as the epsilon channels
mat (ExternalEpsChannel deltaEps) = do
  eps <- mat (Scalar_ParityEven_Singlet deltaEps)
  ext <- mat ExternalOpChannel
  return $ directSum ext eps

getEpsMat
  :: (BlockFetchContext B3d.Block3d a f, Applicative f, RealFloat a)
  => GNY
  -> f (Matrix 2 2 (Vector a))
getEpsMat g =
  BSDP.getIsolatedMat (blockParams g) (derivsVecGNY g) (runTagged2 (nGroup g, externalDims g) (mat (Scalar_ParityEven_Singlet $ Fixed deltaEps)))
  where
    deltaEps = case objective g of
      FeasibilityEps delta _ -> delta
      _ -> error "can't call getEpsMat without objective FeasibilityEps"

getExtEpsMat
  :: (BlockFetchContext B3d.Block3d a f, Applicative f, RealFloat a)
  => GNY
  -> f (Matrix 3 3 (Vector a))
getExtEpsMat g =
  BSDP.getIsolatedMat (blockParams g) (derivsVecGNY g) (runTagged2 (nGroup g, externalDims g) (mat (ExternalEpsChannel $ Fixed deltaEps)))
  where
    deltaEps = case objective g of
      FeasibilityExtEps delta _ -> delta
      _ -> error "can't call getExtEpsMat without objective FeasibilityExtEps"

bulkConstraints :: GNY -> [BulkConstraint]
bulkConstraints f = do
  parity <- [minBound .. maxBound]
  oNRep <- [minBound .. maxBound]
  l <- case (parity, oNRep) of
    (_, ONVector)                    -> filter (not . HI.isInteger) (spins f)
    (B3d.ParityEven,ONSinglet)       -> filter HI.isEvenInteger (spins f)
    (B3d.ParityOdd, ONSinglet)       -> filter HI.isInteger (spins f)
    (B3d.ParityEven,ONSymTensor)     -> filter HI.isEvenInteger (spins f)
    (B3d.ParityOdd, ONSymTensor)     -> filter HI.isInteger (spins f)
    (B3d.ParityEven,ONAntiSymTensor) -> filter HI.isOddInteger (spins f)
    (B3d.ParityOdd, ONAntiSymTensor) -> filter (\x -> (x/=0) && (HI.isInteger x)) (spins f)
  (delta, range) <- listDeltas (ChannelType l parity oNRep) (spectrum f)
  let iConformalRep = B3d.ConformalRep delta l
  pure $ case (parity, HI.isEvenInteger l, l, oNRep) of
    (B3d.ParityEven, True,  0, ONSinglet)       -> BulkConstraint range $ Scalar_ParityEven_Singlet delta
    (B3d.ParityEven, True,  _, ONSinglet)       -> BulkConstraint range $ SpinEven_ParityEven_Singlet iConformalRep
    (B3d.ParityOdd,  True,  _, ONSinglet)       -> BulkConstraint range $ SpinEven_ParityOdd_Singlet iConformalRep
    (B3d.ParityOdd,  False, _, ONSinglet)       -> BulkConstraint range $ SpinOdd_ParityOdd_Singlet iConformalRep
    (B3d.ParityEven, True,  0, ONSymTensor)     -> BulkConstraint range $ Scalar_ParityEven_TensorSym delta
    (B3d.ParityEven, True,  _, ONSymTensor)     -> BulkConstraint range $ SpinEven_ParityEven_TensorSym iConformalRep
    (B3d.ParityOdd,  True,  _, ONSymTensor)     -> BulkConstraint range $ SpinEven_ParityOdd_TensorSym iConformalRep
    (B3d.ParityOdd,  False, _, ONSymTensor)     -> BulkConstraint range $ SpinOdd_ParityOdd_TensorSym iConformalRep
    (B3d.ParityEven, False, _, ONAntiSymTensor) -> BulkConstraint range $ SpinOdd_ParityEven_TensorAntiSym iConformalRep
    (B3d.ParityOdd,  False, _, ONAntiSymTensor) -> BulkConstraint range $ SpinOdd_ParityOdd_TensorAntiSym iConformalRep
    (B3d.ParityOdd,  True,  _, ONAntiSymTensor) -> BulkConstraint range $ SpinEven_ParityOdd_TensorAntiSym iConformalRep
    (B3d.ParityEven, False, _, ONVector)        -> BulkConstraint range $ ParityEven_Vector iConformalRep
    (B3d.ParityOdd,  False, _, ONVector)        -> BulkConstraint range $ ParityOdd_Vector iConformalRep
    _ -> error $ "Internal representation disallowed: " ++ show((parity,l,oNRep))

gnySDP
  :: forall a m.
     ( Binary a, Typeable a, RealFloat a, Applicative m
     , BlockFetchContext B3d.Block3d a m
     )
  => GNY
  -> SDP.SDP m a
gnySDP f@GNY{..} = runTagged2 (nGroup,externalDims) $ do
  let dv = derivsVecGNY f
  bulk   <- for (bulkConstraints f) $
    \(BulkConstraint range c) ->
      BSDP.bootstrapConstraint blockParams dv range <$> mat c
  extMat <- mat ExternalOpChannel
  unit   <- mat IdentityChannel
  stress <- mat StressTensorChannel
  current <- mat CurrentChannel
  let
    stressCons = BSDP.isolatedConstraint blockParams dv stress --Isolated stress
    extCons = BSDP.isolatedConstraint blockParams dv extMat --Isolated stress
    currentCons = BSDP.isolatedConstraint blockParams dv current
  (cons, obj, norm) <- case objective of
    Feasibility -> pure $
      ( bulk ++ [extCons, stressCons, currentCons]
      , BSDP.bootstrapObjective blockParams dv $ zero `asTypeOf` unit
      , BSDP.bootstrapNormalization blockParams dv $ unit
      )
    StressTensorOPEBound dir -> pure $
      ( bulk ++ [extCons, currentCons]
      , BSDP.bootstrapObjective blockParams dv $ unit
      , BSDP.bootstrapNormalization blockParams dv $ boundDirSign dir *^ stress
      )
      -- ConservedCurrentOPEBound dir ->
      --   ( bulk ++ [extCons, stressCons]
      --   , BSDP.bootstrapObjective blockParams dv $ unit
      --   , BSDP.bootstrapNormalization blockParams dv $ boundDirSign dir *^ current
      --   )
    ExternalOPEBound dir -> pure $
      ( bulk ++ [stressCons, currentCons]
      , BSDP.bootstrapObjective blockParams dv $ unit
      , BSDP.bootstrapNormalization blockParams dv $ boundDirSign dir *^ extMat
      )
    ExternalOPEBoundEps dir deltaEps lambda -> do
      epsMat <- mat (Scalar_ParityEven_Singlet $ Fixed deltaEps)
      let epsCons = BSDP.isolatedConstraint blockParams dv $
                    bilinearPair (fmap fromRational lambda) epsMat
      pure $
        ( bulk ++ [stressCons, currentCons, epsCons]
        , BSDP.bootstrapObjective blockParams dv $ unit
        , BSDP.bootstrapNormalization blockParams dv $ boundDirSign dir *^ extMat
        )
    FeasibilityEps deltaEps lambda -> do
      epsMat <- mat (Scalar_ParityEven_Singlet $ Fixed deltaEps)
      let epsCons = BSDP.isolatedConstraint blockParams dv $
                    bilinearPair (fmap fromRational lambda) epsMat
      pure $
        ( bulk ++ [extCons, stressCons, currentCons
                  , epsCons
                  ]
        , BSDP.bootstrapObjective blockParams dv $ zero `asTypeOf` unit
        , BSDP.bootstrapNormalization blockParams dv $ unit
        )
    FeasibilityExtEps deltaEps lambda -> do
      extEpsMat <- mat (ExternalEpsChannel $ Fixed deltaEps)
      let extEpsCons = BSDP.isolatedConstraint blockParams dv $
                    bilinearPair (fmap fromRational lambda) extEpsMat
      pure $
        ( bulk ++ [stressCons, currentCons
                  , extEpsCons
                  ]
        , BSDP.bootstrapObjective blockParams dv $ zero `asTypeOf` unit
        , BSDP.bootstrapNormalization blockParams dv $ unit
        )
  return $ SDP.SDP
    { SDP.sdpObjective     = obj
    , SDP.sdpNormalization = norm
    , SDP.sdpMatrices      = cons
    }

instance ToSDP GNY where
  type SDPFetchKeys GNY = '[ B3d.BlockTableKey ]
  toSDP = gnySDP

instance SDPFetchBuildConfig GNY where
  sdpFetchConfig _ _ cftBoundFiles =
    liftIO . B3d.readBlockTable (blockDir cftBoundFiles) :&: FetchNil
  sdpDepBuildChain _ bConfig cftBoundFiles =
    SomeBuildChain $ noDeps $ block3dBuildLink bConfig cftBoundFiles

instance Static (Binary GNY)              where closureDict = cPtr (static Dict)
instance Static (Show GNY)                where closureDict = cPtr (static Dict)
instance Static (ToSDP GNY)               where closureDict = cPtr (static Dict)
instance Static (ToJSON GNY)              where closureDict = cPtr (static Dict)
instance Static (SDPFetchBuildConfig GNY) where closureDict = cPtr (static Dict)
instance Static (BuildInJob GNY)          where closureDict = cPtr (static Dict)
