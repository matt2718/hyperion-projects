{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE LambdaCase             #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE MultiWayIf             #-}
{-# LANGUAGE NamedFieldPuns         #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TupleSections          #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Bounds.ONVec where

import           Bounds.GNY                             (ON3PtStruct (..),
                                                         ON4PtStruct (..),
                                                         ONRep (..),
                                                         evalONBlock)
import           Control.Monad.IO.Class                 (liftIO)
import           Data.Aeson                             (FromJSON, FromJSONKey,
                                                         ToJSON, ToJSONKey)
import           Data.Binary                            (Binary)
import           Data.Data                              (Typeable)
import           Data.Matrix.Static                     (Matrix)
import           Data.Reflection                        (Reifies, reflect)
import           Data.Tagged                            (Tagged)
import           Data.Traversable                       (for)
import           Data.Vector                            (Vector)
import           GHC.Generics                           (Generic)
import           GHC.TypeNats                           (KnownNat)
import           Hyperion                               (Dict (..), Static (..),
                                                         cPtr)
import           Hyperion.Bootstrap.CFTBound            (BuildInJob,
                                                         SDPFetchBuildConfig (..),
                                                         ToSDP (..), blockDir)
import           Linear.V                               (V)
import           SDPB.Blocks                            (BlockFetchContext,
                                                         Coordinate (ZZb),
                                                         CoordinateDir (CrossingDir),
                                                         CrossingMat,
                                                         Derivative (..),
                                                         Sign (..),
                                                         TaylorCoeff (..),
                                                         Taylors, crossEven,
                                                         crossOdd, unzipBlock,
                                                         zzbTaylors,
                                                         zzbTaylorsAll)
import qualified SDPB.Blocks                            as Blocks
import qualified SDPB.Blocks.ScalarBlocks               as SB
import           SDPB.Blocks.ScalarBlocks.Build         (scalarBlockBuildLink)
import qualified SDPB.Bounds.BootstrapSDP               as BSDP
import           SDPB.Bounds.BoundDirection             (BoundDirection,
                                                         boundDirSign)
import           SDPB.Bounds.Crossing.CrossingEquations (FourPointFunctionTerm,
                                                         HasRep (..),
                                                         OPECoefficient,
                                                         OPECoefficientExternal,
                                                         ThreePointStructure (..),
                                                         crossingMatrix,
                                                         crossingMatrixExternal,
                                                         derivsVec, map4pt,
                                                         mapBlocksFreeVect,
                                                         opeCoeffExternalSimple,
                                                         opeCoeffGeneric_,
                                                         opeCoeffIdentical_,
                                                         runTagged2)
import           SDPB.Bounds.Spectrum                   (DeltaRange, Spectrum,
                                                         listDeltas)
import           SDPB.Build                             (FetchConfig (..),
                                                         SomeBuildChain (..),
                                                         noDeps)
import           SDPB.Math.FreeVect                     (FreeVect, vec)
import           SDPB.Math.Linear.Literal               (toV)
import qualified SDPB.Math.Linear.Util                  as L
import           SDPB.Math.VectorSpace                  (bilinearPair, zero,
                                                         (*^))
import qualified SDPB.Math.VectorSpace                  as VS
import qualified SDPB.SDP.Types                         as SDP

data ExternalOp s = S | Phi
  deriving (Show, Eq, Ord, Enum, Bounded)

data ExternalDims = ExternalDims
  { deltaS   :: Rational
  , deltaPhi :: Rational
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

instance (Reifies s ExternalDims) => HasRep (ExternalOp s) (SB.ScalarRep 3, ONRep) where
  rep x@S   = (SB.ScalarRep $ deltaS (reflect x), ONSinglet)
  rep x@Phi = (SB.ScalarRep $ deltaPhi (reflect x), ONVector)

-- | An instance for creating O(N) three-point structure
-- adapted from Rajeev's QED3 code and GNY
instance ThreePointStructure
  (SB.Standard3PtStruct 3, ON3PtStruct n)
  ()
  (SB.ScalarRep 3, ONRep)
  (SB.SymTensorRep 3, ONRep) where
  makeStructure (l1,r1) (l2,r2) (l3,r3) () =
    (makeStructure l1 l2 l3 (), ON3PtStruct r1 r2 r3)

instance ThreePointStructure
  (SB.Standard3PtStruct 3, ON3PtStruct n)
  ()
  (SB.ScalarRep 3, ONRep)
  (SB.ScalarRep 3, ONRep) where
  makeStructure (l1,r1) (l2,r2) (l3,r3) () =
    (makeStructure l1 l2 l3 (), ON3PtStruct r1 r2 r3)


data ChannelType = ChannelType Int ONRep
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON, ToJSONKey, FromJSONKey)

data ONVec = ONVec
  { externalDims :: ExternalDims
  , nGroup       :: Rational
  , spectrum     :: Spectrum ChannelType
  , objective    :: Objective
  , spins        :: [Int]
  , blockParams  :: SB.ScalarBlockParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility (Maybe (V 2 Rational))
  | StressTensorOPEBound BoundDirection
  | GFFNavigator (Maybe (V 2 Rational))
--  | ExternalOPEBound BoundDirection
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

-- from QED 3
-- | Crossing equation for a four-point function of scalars
-- (first param can be S or phi, it doesn't matter)
crossingEqSSSS
  :: forall s a b .
     ExternalOp s
  -> Sign 'CrossingDir
  -> FourPointFunctionTerm (ExternalOp s) (SB.Standard4PtStruct, Blocks.DerivMultiplier) b a
  -> V 1 (Taylors 'ZZb, FreeVect b a)
crossingEqSSSS op sign g = toV
  (zzbTaylors (sign<>crossOdd), g op op op op (SB.Standard4PtStruct, Blocks.SChannel))

-- | Crossing equation for four scalars of two varieties.
crossingEqSSSS2
  :: forall s a b . (Ord b, Fractional a, Eq a)
  => ExternalOp s
  -> ExternalOp s
  -> Sign 'CrossingDir
  -> FourPointFunctionTerm (ExternalOp s) (SB.Standard4PtStruct, Blocks.DerivMultiplier) b a
  -> V 2 (Taylors 'ZZb, FreeVect b a)
crossingEqSSSS2 o1 o2 sign g = toV
  ( (zzbTaylors (sign<>crossOdd), gS o1 o2 o1 o2)
  , (zzbTaylorsAll, gS o2 o2 o1 o1 - gT o1 o2 o2 o1) )
  where
    gS a b c d = g a b c d (SB.Standard4PtStruct, Blocks.SChannel)
    gT a b c d = g a b c d (SB.Standard4PtStruct, Blocks.TChannel)

-- | Crossing equations of the GNY model with N /= 1 (adapted from GNY)
crossingEqsONVec
  :: forall n s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s)
     ((SB.Standard4PtStruct, Blocks.DerivMultiplier), ON4PtStruct n) b a
  -> V 6 (Taylors 'ZZb, FreeVect b a) -- would be 7, but can use +-
crossingEqsONVec g =
  crossingEqSSSS Phi crossEven (map4pt (,Q3)     g)     L.++
  crossingEqSSSS Phi crossEven (map4pt (,QPlus)  g)     L.++
  crossingEqSSSS Phi crossOdd  (map4pt (,QMinus)     g) L.++
  crossingEqSSSS S crossEven (map4pt (,Unique) g) L.++ -- figure out what this is later
  crossingEqSSSS2 Phi S crossEven  (map4pt (,Unique) g)

derivsVecONVec :: ONVec -> V 6 (Vector (TaylorCoeff (Derivative 'ZZb)))
derivsVecONVec ONVec{..} =
  fmap ($ nmax) (derivsVec crossingEqsONVec)
  where
    nmax = (SB.nmax :: SB.ScalarBlockParams -> Int) blockParams

-- | A channel with crossing matrices of size jxj, mostly copied from Ising
data Channel j where
  SingletCh       :: SB.SymTensorRep 3 -> Channel 2
  SymTensorCh     :: SB.SymTensorRep 3 -> Channel 1
  AntiSymTensorCh :: SB.SymTensorRep 3 -> Channel 1
  VectorCh        :: SB.SymTensorRep 3 -> Channel 1
  IdentityChannel     :: Channel 1
  StressTensorChannel :: Channel 1
  CurrentChannel      :: Channel 1
  ExternalOpChannel   :: Channel 2

onVecCrossingMat
  :: forall j n s a. (Reifies n Rational, KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient (ExternalOp s) (SB.Standard3PtStruct 3, ON3PtStruct n) a)
  -> Tagged '(n,s) (CrossingMat j 6 (SB.ScalarBlock 3) a)
onVecCrossingMat channel =
  pure $ mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrix channel (crossingEqsONVec @n @s)
  where
    evalFlavorCoeff (b,f) = evalONBlock f *^ vec (SB.ScalarBlock b)

mat
  :: forall n s a j . (Reifies n Rational, Reifies s ExternalDims, Fractional a, Eq a)
  => Channel j
  -> Tagged '(n,s) (CrossingMat j 6 (SB.ScalarBlock 3) a)

mat (SingletCh iConfRep) = onVecCrossingMat $ toV
  ( opeCoeffIdentical_ Phi internalRep (vec ())
  , opeCoeffIdentical_ S internalRep (vec ())
  )
  where
    internalRep = (iConfRep, ONSinglet)

mat (SymTensorCh iConfRep) = onVecCrossingMat $ toV $
  opeCoeffIdentical_ Phi internalRep (vec ())
  where
    internalRep = (iConfRep, ONSymTensor)

mat (AntiSymTensorCh iConfRep) = onVecCrossingMat $ toV $
  opeCoeffIdentical_ Phi internalRep (vec ())
  where
    internalRep = (iConfRep, ONAntiSymTensor)

mat (VectorCh iConfRep) = onVecCrossingMat $ toV $
  opeCoeffGeneric_ Phi S internalRep
    (vec ())
    ((-1)^l *^ vec ())
  where
    internalRep = (iConfRep, ONVector)
    l = SB.symTensorSpin iConfRep

mat IdentityChannel =
  pure $ mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrix (toV identityOpe) (crossingEqsONVec @n @s)
  where
    evalFlavorCoeff (b,f) = evalONBlock f *^ vec (SB.ScalarBlock b)
    identityOpe o1 o2
      | o1 == o2  = vec (confStruct, on3PtStruct)
      | otherwise = 0
      where
        (confRep, onRep) = rep @(ExternalOp s) o1
        confStruct = SB.Standard3PtStruct confRep confRep $ SB.SymTensorRep (Blocks.Fixed 0) 0
        on3PtStruct = ON3PtStruct onRep onRep ONSinglet

mat StressTensorChannel = onVecCrossingMat (toV stressTensorOpe)
  where
    stressTensorRep = SB.SymTensorRep (Blocks.RelativeUnitarity 0) 2
    stressTensorOpe o1 o2
      | o1 == o2  =
        fromRational (SB.scalarDelta confRep) *^
        vec (confStruct, on3PtStruct)
      | otherwise = 0
      where
        (confRep, onRep) = rep @(ExternalOp s) o1
        confStruct = SB.Standard3PtStruct confRep confRep stressTensorRep
        on3PtStruct = ON3PtStruct onRep onRep ONSinglet

mat ExternalOpChannel =
  pure $ mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrixExternal opeCoefficients (crossingEqsONVec @n) [S, Phi]
  where
    evalFlavorCoeff (b,f) = evalONBlock f *^ vec (SB.ScalarBlock b)
    opeCoefficients :: V 2 (OPECoefficientExternal (ExternalOp s)
                            (SB.Standard3PtStruct 3, ON3PtStruct n)
                            a)
    opeCoefficients = toV ( opeCoeffExternalSimple Phi Phi S (vec ())
                          , opeCoeffExternalSimple S   S   S (vec ())
                          )

mat CurrentChannel = mat (AntiSymTensorCh currentRep)
  where
    currentRep = SB.SymTensorRep (Blocks.RelativeUnitarity 0) 1

data BulkConstraint where
   BulkConstraint :: (KnownNat j)
     => DeltaRange -- ^ Isolated or Continuum constraint
     -> Channel j -- ^ The associated Channel
     -> BulkConstraint

getExternalMat
  :: (Blocks.BlockFetchContext (SB.ScalarBlock 3) a f, Applicative f, RealFloat a)
  => ONVec
  -> f (Matrix 2 2 (Vector a))
getExternalMat onv@ONVec{..} =
  BSDP.getIsolatedMat blockParams (derivsVecONVec onv) $
  runTagged2 (nGroup, externalDims) (mat ExternalOpChannel)

bulkConstraints :: ONVec -> [BulkConstraint]
bulkConstraints f = do
  oNRep <- [minBound .. maxBound]
  l <- case oNRep of
    ONSinglet       -> filter even (spins f)
    ONSymTensor     -> filter even (spins f)
    ONAntiSymTensor -> filter (not . even) (spins f)
    ONVector        -> (spins f)
  (delta, range) <- listDeltas (ChannelType l oNRep) (spectrum f)
  let iConformalRep = SB.SymTensorRep delta l

  pure $ case oNRep of
    ONSinglet       -> BulkConstraint range $ SingletCh iConformalRep
    ONSymTensor     -> BulkConstraint range $ SymTensorCh iConformalRep
    ONAntiSymTensor -> BulkConstraint range $ AntiSymTensorCh iConformalRep
    ONVector        -> BulkConstraint range $ VectorCh iConformalRep
--    _  -> error $ "Internal representation disallowed: " ++ show(oNRep)

onVecSDP
  :: forall a m.
     ( Binary a, Typeable a, RealFloat a, Applicative m
     , BlockFetchContext (SB.ScalarBlock 3) a m
     )
  => ONVec
  -> SDP.SDP m a
onVecSDP f@ONVec{..} = runTagged2 (nGroup,externalDims) $ do
  let dv = derivsVecONVec f
  bulk   <- for (bulkConstraints f) $
    \(BulkConstraint range c) ->
      BSDP.bootstrapConstraint blockParams dv range <$> mat c
  extMat  <- mat ExternalOpChannel
  unit    <- mat IdentityChannel
  stress  <- mat StressTensorChannel
  current <- mat CurrentChannel
  let
    stressCons = BSDP.isolatedConstraint blockParams dv stress --Isolated stress
--    extCons = BSDP.isolatedConstraint blockParams dv extMat --Isolated stress
    extCons mLambda = case mLambda of
      Nothing     -> BSDP.isolatedConstraint blockParams dv extMat
      Just lambda -> BSDP.isolatedConstraint blockParams dv $
                     bilinearPair (fmap fromRational lambda) extMat
    currentCons = BSDP.isolatedConstraint blockParams dv current
  (cons, obj, norm) <- case objective of
    Feasibility mLambda -> pure
      ( bulk ++ [extCons mLambda, stressCons, currentCons]
      , zero `asTypeOf` unit
      , unit
      )
    StressTensorOPEBound dir -> pure
      ( bulk ++ [extCons Nothing, currentCons]
      , unit
      , boundDirSign dir *^ stress
      )
    GFFNavigator mLambda -> do
      let ExternalDims { deltaS, deltaPhi } = externalDims
      phiSqSinglet <- bilinearPair (toV ((sqrt 2) / (sqrt $ fromRational nGroup), 0)) <$>
                      mat (SingletCh (SB.SymTensorRep (Blocks.Fixed (2*deltaPhi)) 0))
      phiSqSym     <- bilinearPair (toV (1 / sqrt 2)) <$>
                      mat (SymTensorCh (SB.SymTensorRep (Blocks.Fixed (2*deltaPhi)) 0))
      sSq   <- bilinearPair (toV (0, sqrt 2)) <$>
               mat (SingletCh (SB.SymTensorRep (Blocks.Fixed (2*deltaS)) 0))
      phiS  <- bilinearPair (toV 1) <$>
               mat (VectorCh (SB.SymTensorRep (Blocks.Fixed (deltaS+deltaPhi)) 0))
      let gff = VS.sum [phiSqSinglet, phiSqSym, sSq, phiS]
      pure
        ( bulk ++ [extCons mLambda, stressCons, currentCons]
        , unit
        , (-1) *^ gff
        )
  return $ SDP.SDP
    { SDP.sdpObjective     = BSDP.bootstrapObjective blockParams dv obj
    , SDP.sdpNormalization = BSDP.bootstrapNormalization blockParams dv norm
    , SDP.sdpMatrices      = cons
    }
-- ConservedCurrentOPEBound dir ->
--   ( bulk ++ [extCons, stressCons]
--   , BSDP.bootstrapObjective blockParams dv $ unit
--   , BSDP.bootstrapNormalization blockParams dv $ boundDirSign dir *^ current
--   )
--      ExternalOPEBound dir ->
--        ( bulk ++ [stressCons, currentCons]
--        , BSDP.bootstrapObjective blockParams dv $ unit
--        , BSDP.bootstrapNormalization blockParams dv $ boundDirSign dir *^ extMat
--        )      GFFNavigator mLambda -> do

instance ToSDP ONVec where
  type SDPFetchKeys ONVec = '[ SB.BlockTableKey ]
  toSDP = onVecSDP

instance SDPFetchBuildConfig ONVec where
  sdpFetchConfig _ _ cftBoundFiles =
    liftIO . SB.readBlockTable (blockDir cftBoundFiles) :&: FetchNil
  sdpDepBuildChain _ bConfig cftBoundFiles =
    SomeBuildChain $ noDeps $ scalarBlockBuildLink bConfig cftBoundFiles False

instance Static (Binary ONVec)              where closureDict = cPtr (static Dict)
instance Static (Show ONVec)                where closureDict = cPtr (static Dict)
instance Static (ToSDP ONVec)               where closureDict = cPtr (static Dict)
instance Static (ToJSON ONVec)              where closureDict = cPtr (static Dict)
instance Static (SDPFetchBuildConfig ONVec) where closureDict = cPtr (static Dict)
instance Static (BuildInJob ONVec)          where closureDict = cPtr (static Dict)
