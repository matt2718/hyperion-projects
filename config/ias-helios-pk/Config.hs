-- To use, create a symlink 'app/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}

module Config where

import Hyperion.Bootstrap.Main (HyperionBootstrapConfig (..))

config :: HyperionBootstrapConfig
config = HyperionBootstrapConfig
  { -- Email address for cluster notifications
    emailAddr = Just "pkravchuk@ias.edu"

    -- Working directory for data files
  , dataDir = "/data/pkravchuk/hyperion-dsd/data"

    -- Save logs to this directory
  , logDir = "/data/pkravchuk/hyperion-dsd/logs"

    -- Directory for databases used during computation
  , databaseDir = "/data/pkravchuk/hyperion-dsd/databases"

    -- Hyperion makes a (temporary) copy of its executable in order to run
    -- remote copies of itself. Store executables in this directory
  , execDir = "/data/pkravchuk/hyperion-dsd/executables"

    -- Directory for SLURM jobs
  , jobDir = "/data/pkravchuk/hyperion-dsd/jobs"

    -- Path to a script that does srun sdpb, after loading
    -- appropriate modules and setting environment variables
  , srunSdpbExecutable = "/home/pkravchuk/repos/hyperion-projects/config/ias-helios-pk/srun_sdpb.sh"

    -- Path to a script that runs pvm2sdp, after loading
    -- appropriate modules and setting environment variables
  , srunSdp2inputExecutable = "/home/pkravchuk/repos/hyperion-projects/config/ias-helios-pk/srun_sdp2input.sh"

    -- Path to a script that runs sdpb (without srun), after loading
    -- appropriate modules and setting environment variables
  , sdpbExecutable = "/home/pkravchuk/bin/sdpb"

    -- Path to a script that runs pvm2sdp (without srun), after loading
    -- appropriate modules and setting environment variables
  , sdp2inputExecutable = "/home/pkravchuk/bin/sdp2input"

    -- Path to a script that runs scalar_blocks, after loading appropriate
    -- modules and setting environment variables
  , scalarBlocksExecutable = "/home/pkravchuk/bin/scalar_blocks"

    -- Path to a script that runs blocks_3d, after loading appropriate
    -- modules and setting environment variables
  , blocks3dExecutable = "/home/pkravchuk/bin/blocks_3d"

    -- Path to a script that runs blocks_4d, after loading appropriate
    -- modules and setting environment variables
  , blocks4dExecutable = "/home/pkravchuk/bin/blocks_4d"

    -- Path to a script that runs seed_blocks, after loading appropriate
    -- modules and setting environment variables
  , seeds4dExecutable = "/home/pkravchuk/bin/seed_blocks"

    -- Path to the qdelaunay executable (needed for Delaunay triangulation searches)
  , qdelaunayExecutable = "/usr/bin/qdelaunay"

    -- Path to a script that runs tiptop
  , tiptopExecutable = ""

    -- Command to use to run the hyperion program. If no option is
    -- specified, hyperion will copy its own executable to a safe place and
    -- run that. Default: Nothing
  , hyperionCommand = Nothing

    -- Default SLURM queue (partition) to submit jobs to. Default: Nothing
  , defaultSlurmPartition = Nothing

    -- Default SLURM account. Default: Nothing
  , defaultSlurmAccount = Nothing

    -- Default command for running ssh
  , sshRunCommand = Just ("ssh", ["-f", "-o", "StrictHostKeyChecking no"])
  }
